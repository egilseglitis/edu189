var gulp = require('gulp');
var sass = require('gulp-sass');
var livereload = require('gulp-livereload');
var  concat = require('gulp-concat');

gulp.task('sass', function()
{
    return gulp.src('app/design/frontend/Edu/default/web/sass/**/*.scss')
    .pipe(sass())
    .pipe(concat('styles.css'))
    .pipe(gulp.dest('app/design/frontend/Edu/default/web/css/'))
    .pipe(livereload());
});

gulp.task('watch', function()
{
    livereload.listen();
    gulp.watch('app/design/frontend/Edu/default/web/sass/**/*.scss', ['sass']);
});
