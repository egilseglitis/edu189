<?php

/**
    Scandi_DeliveryPage

    @category    Scandi
    @package     Scandi_DeliveryPage
    @author      Egils Eglitis <info@scandiweb.com>
    @copyright   Copyright (c) 2019 Scandiweb, Ltd (https://scandiweb.com)
 */

namespace Scandi\DeliveryPage\Setup;

use Magento\Cms\Model\PageFactory;
use Magento\Cms\Model\BlockFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * Upgrade Data script
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var BlockFactory
     */
    private $blockFactory;

    /**
     * @var DirectoryList
     */
    private $directoryList;

    /**
     * @var PageFactory
     */
    private $pageFactory;

    /**
     * UpgradeData constructor.
     * @param BlockFactory $blockFactory
     * @param DirectoryList $directoryList
     * @param PageFactory $pageFactory
     */
    public function __construct(BlockFactory $blockFactory, DirectoryList $directoryList, PageFactory $pageFactory)
    {
        $this->blockFactory = $blockFactory;
        $this->directoryList = $directoryList;
        $this->pageFactory = $pageFactory;
    }

    /**
     * Creates/Updates a CMS page and block
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws NoSuchEntityException
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $pathToFrontend = $this->directoryList->getRoot() . '/app/code/Scandi/DeliveryPage/view/frontend';
        $pageContentPath = $pathToFrontend . '/templates/delivery-page.phtml';

        $blocks = [
            'delivery' => [
                'title' => 'Delivery Information',
                'identifier' => 'delivery-information',
                'path' => $pathToFrontend . '/templates/delivery.html'
            ],
            'delivery-top' => [
                'title' => 'Delivery Information Top',
                'identifier' => 'delivery-information-top',
                'path' => $pathToFrontend . '/templates/top.html'
            ],
            'delivery-courier' => [
                'title' => 'Delivery Information Courier',
                'identifier' => 'delivery-information-courier',
                'path' => $pathToFrontend . '/templates/courier.html'
            ],
            'delivery-local' => [
                'title' => 'Delivery Information Local',
                'identifier' => 'delivery-information-local',
                'path' => $pathToFrontend . '/templates/local.html'
            ],
            'delivery-half-columns' => [
                'title' => 'Delivery Information Half Columns',
                'identifier' => 'delivery-information-half-columns',
                'path' => $pathToFrontend . '/templates/half-columns.html'
            ],
            'delivery-full-paragraph' => [
                'title' => 'Delivery Information Full Paragraph',
                'identifier' => 'delivery-information-full-paragraph',
                'path' => $pathToFrontend . '/templates/full-paragraph.html'
            ],
            'delivery-table' => [
                'title' => 'Delivery Information Table',
                'identifier' => 'delivery-information-table',
                'path' => $pathToFrontend . '/templates/table.html'
            ],
        ];

        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.0') < 0) {
            $this->setPage(
                'Delivery',
                'delivery',
                $pageContentPath
            );

            foreach ($blocks as $block) {
                $this->setBlock(
                    $block['title'],
                    $block['identifier'],
                    $block['path']
                );
            }
        }

        $setup->endSetup();
    }

    /**
     * Returns file content in a string form
     * @param $path
     * @return false|string
     * @throws NoSuchEntityException
     */
    public function getContent($path)
    {
        if (file_exists($path)) {
            return file_get_contents($path);
        } else {
            throw new NoSuchEntityException(__('Could not find the file.', $path));
        }
    }

    /**
     * Creates/Updates a CMS block
     * @param $title
     * @param $identifier
     * @param $path
     * @throws NoSuchEntityException
     */
    public function setBlock($title, $identifier, $path)
    {
        $this->blockFactory->create()->load($identifier)
            ->setTitle($title)
            ->setIdentifier($identifier)
            ->setIsActive(true)
            ->setStores([0])
            ->setSortOrder(0)
            ->setContent($this->getContent($path))
            ->save();
    }

    /**
     * Creates/Updates a CMS page
     * @param $title
     * @param $identifier
     * @param $path
     * @throws NoSuchEntityException
     */
    public function setPage($title, $identifier, $path)
    {
        $this->pageFactory->create()->load($identifier)
            ->setTitle($title)
            ->setIdentifier($identifier)
            ->setIsActive(true)
            ->setPageLayout('1column')
            ->setStores([0])
            ->setContent($this->getContent($path))
            ->save();
    }
}
