<?php namespace Scandi\LanguageSwitch\Setup;
/**
  scandi_default

  @category    scandi
  @package     scandi_languageswitch
  @author      Egils Eglitis <info@scandiweb.com>
  @copyright   Copyright (c) 2018 Scandiweb, Ltd (https://scandiweb.com)
*/

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Store\Model\ScopeInterface;
use \Magento\Store\Model\StoreRepository;
use Magento\Store\Model\ResourceModel\Store;
use \Magento\Store\Model\StoreFactory;

class UpgradeData implements UpgradeDataInterface
{
    private $configWriter;
    private $storeResourceModel;
    private $storeManager;
    private $storeFactory;

    public function __construct(
        WriterInterface $writer,
        Store $storeRM,
        StoreRepository $storeManager,
        StoreFactory $storeFactory
    ){
        $this->configWriter = $writer;
        $this->storeResourceModel = $storeRM;
        $this->storeManager = $storeManager;
        $this->storeFactory = $storeFactory;
    }

    public function upgrade(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ){
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.0')) {
            //Create a new Store View and assign a key to it
            $store = $this->storeFactory->create();
            $store->setCode('DE');

            //Get the Default Store as the German Store View is in Default Store
            $defaultStore = $this->storeManager->get('default');
            $defaultStoreID = $defaultStore->getId();

            //Get needed attributes from the Default Store to be used for German Store View
            $storeWebsiteID = $defaultStore->getWebsiteId();
            $storeGroupID = $defaultStore->getStoreGroupId();

            if (!$store->getId()) {
                //Add attributes to the new Store View and save it
                $store->setWebsiteId($storeWebsiteID);
                $store->setGroupId($storeGroupID);
                $store->setCode('DE');
                $store->setName('German Site View');
                $store->setSortOrder('0');
                $store->setIsActive('1');
                $this->storeResourceModel->save($store);

                $storeID = $this->storeManager->get('DE')->getId();

                //Add Store view related data to core config
                $this->configWriter->save('general/country/default', 'DE', ScopeInterface::SCOPE_STORES, $storeID);
                $this->configWriter->save('general/locale/code', 'de_DE', ScopeInterface::SCOPE_STORES, $storeID);
                $this->configWriter->save('general/locale/weight_unit', 'kgs', ScopeInterface::SCOPE_STORES, $storeID);
                $this->configWriter->save('general/locale/firstday', '1', ScopeInterface::SCOPE_STORES, $storeID);
                $this->configWriter->save('currency/options/base', 'EUR', ScopeInterface::SCOPE_STORES, $storeID);
                $this->configWriter->save('currency/options/default', 'EUR', ScopeInterface::SCOPE_STORES, $storeID);
                $this->configWriter->save('currency/options/allow', 'EUR', ScopeInterface::SCOPE_STORES, $storeID);
                $this->configWriter->save('catalog/seo/product_url_suffix', NULL, ScopeInterface::SCOPE_STORES, 0);
                $this->configWriter->save('catalog/seo/category_url_suffix', NULL, ScopeInterface::SCOPE_STORES, 0);
            }
        }
        $setup->endSetup();
    }
}
