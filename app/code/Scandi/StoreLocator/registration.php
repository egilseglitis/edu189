<?php

/**
    Scandi_StoreLocator

    @category    Scandi
    @package     Scandi_StoreLocator
    @author      Egils Eglitis <info@scandiweb.com>
    @copyright   Copyright (c) 2019 Scandiweb, Ltd (https://scandiweb.com)
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Scandi_StoreLocator',
    __DIR__
);
