<?php

/**
Scandi_StoreLocator

@category    Scandi
@package     Scandi_StoreLocator
@author      Egils Eglitis <info@scandiweb.com>
@copyright   Copyright (c) 2019 Scandiweb, Ltd (https://scandiweb.com)
 */

namespace Scandi\StoreLocator\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Store model
 */
class Store extends AbstractModel
{
    const STORE_ID = 'store_id';
    const STORE_NAME = 'store_name';
    const STORE_ADDRESS = 'store_address';
    const MONDAY_WORK_TIME = 'monday_work_time';
    const TUESDAY_WORK_TIME = 'tuesday_work_time';
    const WEDNESDAY_WORK_TIME = 'wednesday_work_time';
    const THURSDAY_WORK_TIME = 'thursday_work_time';
    const FRIDAY_WORK_TIME = 'friday_work_time';
    const SATURDAY_WORK_TIME = 'saturday_work_time';
    const SUNDAY_WORK_TIME = 'sunday_work_time';
    const STORE_SUMMARY = 'store_summary';

    const CACHE_TAG = 'physical_stores';

    protected $_cacheTag = 'physical_stores';

    protected $_eventPrefix = 'physical_stores';

    /**
     * Store constructor
     */
    protected function _construct()
    {
        $this->_init('Scandi\StoreLocator\Model\ResourceModel\Store');
    }

    /**
     * Get store ID
     * @return mixed
     */
    public function getStoreID()
    {
        return $this->getData(self::STORE_ID);
    }

    /**
     * Get Store Name
     * @return mixed
     */
    public function getStoreName()
    {
        return $this->getData(self::STORE_NAME);
    }

    /**
     * Set Store Name
     * @param $storeName
     * @return Store
     */
    public function setStoreName($storeName)
    {
        return $this->setData(self::STORE_NAME, $storeName);
    }

    /**
     * Get Store Address
     * @return mixed
     */
    public function getStoreAddress()
    {
        return $this->getData(self::STORE_ADDRESS);
    }

    /**
     * Set Store Address
     * @param $storeAddress
     * @return Store
     */
    public function setStoreAddress($storeAddress)
    {
        return $this->setData(self::STORE_ADDRESS, $storeAddress);
    }

    /**
     * Get Store's Monday Work Time
     * @return mixed
     */
    public function getMondayWorkTime()
    {
        return $this->getData(self::MONDAY_WORK_TIME);
    }

    /**
     * Set Store's Monday Work Time
     * @param $workTime
     * @return Store
     */
    public function setMondayWorkTime($workTime)
    {
        return $this->setData(self::MONDAY_WORK_TIME, $workTime);
    }

    /**
     * Get Store's Tuesday Work Time
     * @return mixed
     */
    public function getTuesdayWorkTime()
    {
        return $this->getData(self::TUESDAY_WORK_TIME);
    }
    /**
     * Set Store's Tuesday Work Time
     * @param $workTime
     * @return Store
     */
    public function setTuesdayWorkTime($workTime)
    {
        return $this->setData(self::TUESDAY_WORK_TIME, $workTime);
    }

    /**
     * Get Store's Wednesday Work Time
     * @return mixed
     */
    public function getWednesdayWorkTime()
    {
        return $this->getData(self::WEDNESDAY_WORK_TIME);
    }

    /**
     * Set Store's Wednesday Work Time
     * @param $workTime
     * @return Store
     */
    public function setWednesdayWorkTime($workTime)
    {
        return $this->setData(self::WEDNESDAY_WORK_TIME, $workTime);
    }


    /**
     * Get Store's Thursday Work Time
     * @return mixed
     */
    public function getThursdayWorkTime()
    {
        return $this->getData(self::THURSDAY_WORK_TIME);
    }

    /**
     * Set Store's Thursday Work Time
     * @param $workTime
     * @return Store
     */
    public function setThursdayWorkTime($workTime)
    {
        return $this->setData(self::THURSDAY_WORK_TIME, $workTime);
    }

    /**
     * Get Store's Friday Work Time
     * @return mixed
     */
    public function getFridayWorkTime()
    {
        return $this->getData(self::FRIDAY_WORK_TIME);
    }

    /**
     * Set Store's Friday Work Time
     * @param $workTime
     * @return Store
     */
    public function setFridayWorkTime($workTime)
    {
        return $this->setData(self::FRIDAY_WORK_TIME, $workTime);
    }

    /**
     * Get Store's Saturday Work Time
     * @return mixed
     */
    public function getSaturdayWorkTime()
    {
        return $this->getData(self::SATURDAY_WORK_TIME);
    }

    /**
     * Set Store's Saturday Work Time
     * @param $workTime
     * @return Store
     */
    public function setSaturdayWorkTime($workTime)
    {
        return $this->setData(self::SATURDAY_WORK_TIME, $workTime);
    }

    /**
     * Get Store's Sunday Work Time
     * @return mixed
     */
    public function getSundayWorkTime()
    {
        return $this->getData(self::SUNDAY_WORK_TIME);
    }

    /**
     * Set Store's Sunday Work Time
     * @param $workTime
     * @return Store
     */
    public function setSundayWorkTime($workTime)
    {
        return $this->setData(self::SUNDAY_WORK_TIME, $workTime);
    }

    /**
     * Get Store Summary
     * @return mixed
     */
    public function getStoreSummary()
    {
        return $this->getData(self::STORE_SUMMARY);
    }

    /**
     * Set Store Summary
     * @param $storeSummary
     * @return Store
     */
    public function setStoreSummary($storeSummary)
    {
        return $this->setData(self::STORE_SUMMARY, $storeSummary);
    }
}
