<?php

/**
Scandi_StoreLocator

@category    Scandi
@package     Scandi_StoreLocator
@author      Egils Eglitis <info@scandiweb.com>
@copyright   Copyright (c) 2019 Scandiweb, Ltd (https://scandiweb.com)
 */

namespace Scandi\StoreLocator\Model;

use Magento\Ui\DataProvider\AbstractDataProvider;
use Scandi\StoreLocator\Model\ResourceModel\Grid\CollectionFactory;

/**
 * Store data provider
 */
class DataProvider extends AbstractDataProvider
{
    /**
     * DataProvider constructor.
     * @param $name
     * @param $primaryFieldName
     * @param $requestFieldName
     * @param CollectionFactory $contactCollectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
    $name,
    $primaryFieldName,
    $requestFieldName,
    CollectionFactory $contactCollectionFactory,
    array $meta = [],
    array $data = []
    ) {
        $this->collection = $contactCollectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        $items = $this->collection->getItems();
        $this->loadedData = array();

        foreach ($items as $store) {
            $this->loadedData[$store->getId()] = $store->getData();
        }

        return $this->loadedData;

    }
}
