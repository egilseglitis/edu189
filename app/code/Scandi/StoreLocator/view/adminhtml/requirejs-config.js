/**
 Scandi_StoreLocator

 @category    Scandi
 @package     Scandi_StoreLocator
 @author      Egils Eglitis <info@scandiweb.com>
 @copyright   Copyright (c) 2019 Scandiweb, Ltd (https://scandiweb.com)
 */

var config = {
    config: {
        mixins: {
            'Magento_Ui/js/lib/validation/validator': {
                'Scandi_StoreLocator/js/validation-mixin': true
            }
        }
    }
};
