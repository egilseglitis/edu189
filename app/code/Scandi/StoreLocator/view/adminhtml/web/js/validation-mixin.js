/**
 Scandi_StoreLocator

 @category    Scandi
 @package     Scandi_StoreLocator
 @author      Egils Eglitis <info@scandiweb.com>
 @copyright   Copyright (c) 2019 Scandiweb, Ltd (https://scandiweb.com)
 */

define([
    'jquery',
    'jquery/ui',
    'jquery/validate',
    'mage/translate'
], function ($, validator) {
        "use strict";

        return function (validator) {
            validator.addRule(
                'validate-time',
                function (value) {
                    return /^([01][0-9]|2[0-3]):([0-5][0-9]) - ([01][0-9]|2[0-3]):([0-5][0-9])$/.test(value);
                },
                $.mage.__('Please enter correct time.')
            );
            return validator;
        }
    }
);
