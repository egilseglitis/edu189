<?php

/**
Scandi_StoreLocator

@category    Scandi
@package     Scandi_StoreLocator
@author      Egils Eglitis <info@scandiweb.com>
@copyright   Copyright (c) 2019 Scandiweb, Ltd (https://scandiweb.com)
 */

namespace Scandi\StoreLocator\Block;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\Element\Template;
use Scandi\StoreLocator\Model\Store;

/**
 * Class to work with store location
 */
class MapData extends Template
{
    /**
     * @var Store
     */
    private $store;

    /**
     * MapData constructor.
     * @param Context $context
     * @param Store $store
     */
    public function __construct(Context $context, Store $store)
    {
        $this->store = $store;
        parent::__construct($context);

    }

    /**
     * Get collection from database`
     *
     * @return AbstractCollection
     */
    public function getCollection()
    {
        $collection = $this->store->getCollection();
        return $collection;
    }

    /**
     * Get coordinates from address
     * @param $address
     * @return string
     */
    function getCoordinates($address)
    {
        $xml = simplexml_load_file(
            'http://geocode-maps.yandex.ru/1.x/?geocode='
            .urlencode($address).'&key='
            .urlencode('d581cd54-8162-4417-b598-d60a8e34bb16')
            .'&results=1');
        $coordinate = str_replace(' ', ',', $xml->GeoObjectCollection->featureMember->GeoObject->Point->pos);
        $coordinate = implode(', ', array_reverse(explode(',', $coordinate)));

        return $coordinate;
    }
}