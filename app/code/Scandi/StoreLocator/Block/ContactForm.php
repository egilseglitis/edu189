<?php

/**
Scandi_StoreLocator

@category    Scandi
@package     Scandi_StoreLocator
@author      Egils Eglitis <info@scandiweb.com>
@copyright   Copyright (c) 2019 Scandiweb, Ltd (https://scandiweb.com)
 */

namespace Scandi\StoreLocator\Block;

use Magento\Contact\Block\ContactForm as ContactFormBase;
use Magento\Framework\View\Element\Template\Context;

/**
 * Adds MapData to Contact Us page
 */
class ContactForm extends ContactFormBase
{
    /**
     * @var MapData
     */
    private $mapData;

    /**
     * ContactForm constructor.
     * @param MapData $mapData
     * @param Context $context
     * @param array $data
     */
    public function __construct(MapData $mapData, Context $context, array $data = [])
    {
        parent::__construct($context, $data);
        $this->mapData = $mapData;
    }

    /**
     * Get Map Data
     * @return MapData
     */
    public function getMapData()
    {
        return $this->mapData;
    }
}