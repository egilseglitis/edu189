<?php

/**
    Scandi_StoreLocator

    @category    Scandi
    @package     Scandi_StoreLocator
    @author      Egils Eglitis <info@scandiweb.com>
    @copyright   Copyright (c) 2019 Scandiweb, Ltd (https://scandiweb.com)
 */

namespace Scandi\StoreLocator\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * Store UpgradeSchema script
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * Schema Upgrade Script
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.0') < 0) {
            $table = $setup->getConnection()->newTable('physical_stores')
                ->addColumn(
                    'store_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ]
                )
                ->addColumn(
                    'store_name',
                    Table::TYPE_TEXT,
                    null,
                    [
                        'nullable' => false
                    ]
                )
                ->addColumn(
                    'store_address',
                    Table::TYPE_TEXT,
                    null,
                    [
                        'nullable' => false
                    ]
                )
                ->addColumn(
                    'monday_work_time',
                    Table::TYPE_TEXT,
                    null,
                    [
                        'nullable' => false
                    ]
                )
                ->addColumn(
                    'tuesday_work_time',
                    Table::TYPE_TEXT,
                    null,
                    [
                        'nullable' => false
                    ]
                )
                ->addColumn(
                    'wednesday_work_time',
                    Table::TYPE_TEXT,
                    null,
                    [
                        'nullable' => false
                    ]
                )
                ->addColumn(
                    'thursday_work_time',
                    Table::TYPE_TEXT,
                    null,
                    [
                        'nullable' => false
                    ]
                )
                ->addColumn(
                    'friday_work_time',
                    Table::TYPE_TEXT,
                    null,
                    [
                        'nullable' => false
                    ]
                )
                ->addColumn(
                    'saturday_work_time',
                    Table::TYPE_TEXT,
                    null,
                    [
                        'nullable' => false
                    ]
                )
                ->addColumn(
                    'sunday_work_time',
                    Table::TYPE_TEXT,
                    null,
                    [
                        'nullable' => false
                    ]
                )
                ->addColumn(
                    'store_summary',
                    Table::TYPE_TEXT,
                    null,
                    [
                        'nullable' => true
                    ]
                );

            $setup->getConnection()->createTable($table);
        }

        $setup->endSetup();
    }
}
