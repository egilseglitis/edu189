<?php

/**
    Scandi_StoreLocator

    @category    Scandi
    @package     Scandi_StoreLocator
    @author      Egils Eglitis <info@scandiweb.com>
    @copyright   Copyright (c) 2019 Scandiweb, Ltd (https://scandiweb.com)
*/

namespace Scandi\StoreLocator\Controller\Action;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\Result\JsonFactory;
use Scandi\StoreLocator\Model\Store;

/**
 * Store list retrieval
 */
Class StoreSearch extends Action
{
    /**
     * @var JsonFactory
     */
    private $resultJsonFactory;

    /**
     * @var Store
     */
    private $store;

    /**
     * StoreSearch constructor.
     * @param Context $context
     * @param Store $store
     * @param JsonFactory $resultJsonFactory
     */
    public function __construct(Context $context, Store $store, JsonFactory $resultJsonFactory)
    {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->store = $store;
    }

    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        $storeArray = $this->store->getCollection()->getData();
        $addressArray = [];
        $i = 0;

        foreach ($storeArray as $store)
        {
            $addressArray[$i] = $store['store_address'];
            $i++;
        }

        return $result->setData($addressArray);
    }
}
