<?php

/**
Scandi_StoreLocator

@category    Scandi
@package     Scandi_StoreLocator
@author      Egils Eglitis <info@scandiweb.com>
@copyright   Copyright (c) 2019 Scandiweb, Ltd (https://scandiweb.com)
 */

namespace Scandi\StoreLocator\Controller\Adminhtml\Action;

use Magento\Backend\App\Action as MagentoAction;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\Session;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Ui\Component\MassAction\Filter;
use Scandi\StoreLocator\Model\StoreFactory;

abstract class Action extends MagentoAction
{
    /**
     * @var StoreFactory
     */
    protected $storeFactory;

    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var Session
     */
    protected $session;

    /**
     * Action constructor.
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param Session $session
     * @param StoreFactory $storeFactory
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        Session $session,
        StoreFactory $storeFactory
    ) {
        parent::__construct($context);
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->session = $session;
        $this->storeFactory = $storeFactory;
    }

    abstract public function execute();
}