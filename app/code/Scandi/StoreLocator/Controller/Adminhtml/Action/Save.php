<?php

/**
Scandi_StoreLocator

@category    Scandi
@package     Scandi_StoreLocator
@author      Egils Eglitis <info@scandiweb.com>
@copyright   Copyright (c) 2019 Scandiweb, Ltd (https://scandiweb.com)
 */

namespace Scandi\StoreLocator\Controller\Adminhtml\Action;

use Magento\Framework\Controller\ResultFactory;
use Scandi\StoreLocator\Model\StoreFactory;

/**
 * Store saving in the DB
 */
class Save extends Action
{
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();

        if ($data) {
            $data = array_filter(
                $data,
                function ($value) {
                    return $value !== '';
                }
            );

            $store = $this->storeFactory->create();
            $store->setData($data);
            $store->save();
            $this->session->setFormData(false);

            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setPath('storelocator/storelocator/index');

            return $resultRedirect;
        }
    }
}
