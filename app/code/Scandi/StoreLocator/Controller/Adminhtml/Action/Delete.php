<?php

/**
Scandi_StoreLocator

@category    Scandi
@package     Scandi_StoreLocator
@author      Egils Eglitis <info@scandiweb.com>
@copyright   Copyright (c) 2019 Scandiweb, Ltd (https://scandiweb.com)
 */

namespace Scandi\StoreLocator\Controller\Adminhtml\Action;

use Magento\Framework\Controller\ResultFactory;

/**
 * Store deletion from the DB
 */
class Delete extends Action
{
    public function execute()
    {
        $collection = $this->collectionFactory->create();
        $collection = $this->filter->getCollection($collection);

        $store = $this->storeFactory->create();

        foreach ($collection->getAllIds() as $storeId) {
            $store->load($storeId);
            $store->delete();
        }

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath('storelocator/storelocator/index');

        return $resultRedirect;
    }
}
