<?php namespace Scandi\Badge\Setup;
/**
  scandi_default

  @category    scandi
  @package     scandi_badge
  @author      Egils Eglitis <info@scandiweb.com>
  @copyright   Copyright (c) 2018 Scandiweb, Ltd (https://scandiweb.com)
*/

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        //Create a table for badges
        $table = $setup->getConnection()->newTable('badges')
            ->addColumn(
                'badge_id',
                Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary' => true
                ],
                'Badge ID'
            )
            ->addColumn(
                'badge_name',
                Table::TYPE_TEXT,
                255,
                [
                    'nullable' => false
                ],
                'Badge name'
            )
            ->addColumn(
                'badge_image',
                Table::TYPE_TEXT,
                255,
                [
                    'nullable' => false
                ],
                'Badge image name'
            )
            ->addColumn(
                'badge_status',
                Table::TYPE_BOOLEAN,
                null,
                [
                    'nullable' => false,
                ],
                'Badge status'
            )
        ->setComment('Product badge table');
        $setup->getConnection()->createTable($table);

        $setup->endSetup();
    }
}
