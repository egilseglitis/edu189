<?php namespace Scandi\Badge\Controller\Adminhtml\Action;

/**
 * scandi_default
 *
 * @category    scandi
 * @package     scandi_Badge
 * @author      Egils Eglitis <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Ltd (https://scandiweb.com)
 */

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Controller\ResultFactory;

class Save extends BadgeAction
{
    protected $resultPageFactory;
    protected $badgeFactory;
    protected $imageUploader;

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();

        if ($data) {
            $data = array_filter(
                $data,
                function ($value) {
                    return $value !== '';
                }
            );

            if (isset($data['badge_image'][0]['name']) && isset($data['badge_image'][0]['tmp_name'])) {
                $data['badge_image'] = $data['badge_image'][0]['name'];
                $this->imageUploader = ObjectManager::getInstance()->get(
                    'Scandi\Badge\Model\ImageUploader'
                );
                $data['badge_image'] = $this->imageUploader->moveFileFromTmp($data['badge_image']);
            } else {
                $data['badge_image'] = null;
            }

            $badge = $this->badgeFactory->create();
            $badge->setData($data);
            $badge->save();
            $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);

            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setPath('badge/badge/index');

            return $resultRedirect;
        }
    }
}
