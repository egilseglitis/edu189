<?php namespace Scandi\Badge\Controller\Adminhtml\Action;

/**
 * scandi_default
 *
 * @category    scandi
 * @package     scandi_Badge
 * @author      Egils Eglitis <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Ltd (https://scandiweb.com)
 */

use Magento\Framework\Controller\ResultFactory;

class Delete extends BadgeAction
{
    public function execute()
    {
        $collection = $this->collectionFactory->create();
        $collection = $this->filter->getCollection($collection);

        $badgeModel = $this->badgeFactory->create();

        foreach ($collection->getAllIds() as $badgeId) {
            $badgeModel->load($badgeId);
            $badgeModel->delete();
        }

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath('badge/badge/index');

        return $resultRedirect;
    }
}
