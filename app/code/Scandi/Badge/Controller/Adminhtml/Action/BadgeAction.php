<?php namespace Scandi\Badge\Controller\Adminhtml\Action;

/**
 * scandi_default
 *
 * @category    scandi
 * @package     scandi_Badge
 * @author      Egils Eglitis <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Ltd (https://scandiweb.com)
 */

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Ui\Component\MassAction\Filter;
use Scandi\Badge\Model\BadgeFactory;

abstract class BadgeAction extends Action
{
    protected $badgeFactory;
    protected $filter;
    protected $collectionFactory;

    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        BadgeFactory $badgeFactory
    ) {
        parent::__construct($context);
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->badgeFactory = $badgeFactory;
    }

    abstract public function execute();
}
