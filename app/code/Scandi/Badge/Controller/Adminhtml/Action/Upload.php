<?php namespace Scandi\Badge\Controller\Adminhtml\Action;

/**
 * scandi_default
 *
 * @category    scandi
 * @package     scandi_Badge
 * @author      Egils Eglitis <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Ltd (https://scandiweb.com)
 */

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Controller\ResultFactory;
use Scandi\Badge\Model\ImageUploader;

class Upload extends Action
{
    private $imageUploader;
    private $dirList;
    private $imagePath;

    public function __construct(
        Context $context,
        ImageUploader $imageUploader,
        DirectoryList $dirList
    ) {
        parent::__construct($context);
        $this->imageUploader = $imageUploader;
        $this->dirList = $dirList;
        $this->imagePath = 'badges/tmp';
    }

    public function getImagePath()
    {
        return $this->imagePath;
    }

    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;
    }

    public function execute()
    {
        $tmpPath = $this->dirList->getPath('media') . $this->getImagePath();

        if (!is_dir($tmpPath)) {
            $ioAdapter = $this->_objectManager->create('Magento\Framework\Filesystem\Io\File');
            $ioAdapter->mkdir($tmpPath, 0777);
        }

        try {
            $result = $this->imageUploader->saveFileToTmpDir('badge_image');
        } catch (\Exception $e) {
            $result = ['error' => $e->getMessage(), 'errorcode' => $e->getCode()];
        }

        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData($result);
    }
}
