<?php
/**
  scandi_default

  @category    scandi
  @package     scandi_Badge
  @author      Egils Eglitis <info@scandiweb.com>
  @copyright   Copyright (c) 2018 Scandiweb, Ltd (https://scandiweb.com)
*/

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Scandi_Badge',
    __DIR__
);