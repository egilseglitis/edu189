<?php namespace Scandi\Badge\Model;

/**
 * scandi_default
 *
* @category    scandi
* @package     scandi_badge
* @author      Egils Eglitis <info@scandiweb.com>
* @copyright   Copyright (c) 2018 Scandiweb, Ltd (https://scandiweb.com)
 */

use Magento\Framework\Model\AbstractModel;

class Badge extends AbstractModel
{
    const CACHE_TAG = 'badges';
    const BADGE_ID = 'badge_id';
    const BADGE_NAME = 'badge_name';
    const BADGE_IMAGE = 'badge_image';
    const BADGE_STATUS = 'badge_status';

    protected $_cacheTag = 'badges';
    protected $_eventPrefix = 'badges';

    protected function _construct()
    {
        $this->_init('Scandi\Badge\Model\ResourceModel\Badge');
    }

    public function getBadgeId()
    {
        return $this->getData(self::BADGE_ID);
    }

    public function setBadgeId($badgeId)
    {
        return $this->setData(self::BADGE_ID, $badgeId);
    }

    public function getBadgeName()
    {
        return $this->getData(self::BADGE_NAME);
    }

    public function setBadgeName($badgeName)
    {
        return $this->setData(self::BADGE_NAME, $badgeName);
    }

    public function getBadgeImageLink()
    {
        return $this->getData(self::BADGE_IMAGE);
    }

    public function setBadgeImageLink($badgeImageLink)
    {
        return $this->setData(self::BADGE_IMAGE, $badgeImageLink);
    }

    public function getBadgeStatus()
    {
        return $this->getData(self::BADGE_STATUS);
    }

    public function setBadgeStatus($badgeStatus)
    {
        return $this->setData(self::BADGE_STATUS, $badgeStatus);
    }
}
