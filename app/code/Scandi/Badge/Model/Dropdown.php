<?php namespace Scandi\Badge\Model;

/**
 * scandi_default
 *
 * @category    scandi
 * @package     scandi_badge
 * @author      Egils Eglitis <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Ltd (https://scandiweb.com)
 */

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Scandi\Badge\Model\PostFactory;

class Dropdown extends AbstractSource
{
    protected $options;
    private $postFactory;

    public function __construct(PostFactory $postFactory)
    {
        $this->postFactory = $postFactory;
    }

    public function getAllOptions()
    {
        $badges = $this->postFactory->create();
        $badgesCollection = $badges->getCollection();

        if ($this->options === null) {
            $badgeArray[] = array('value'=>null, 'label'=>'No Badge');

            foreach ($badgesCollection as $key => $badge) {

                if($badge->getBadgeStatus() == 1) {
                    $badgeArray[] = array('value' => $badge->getBadgeId(), 'label' => $badge->getBadgeName());
                }

            }
            $this->options = $badgeArray;
        }

        return $this->options;
    }
}
