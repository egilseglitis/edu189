<?php namespace Scandi\Badge\Model\ResourceModel\Post;

/**
 * scandi_default
 *
 * @category    scandi
 * @package     scandi_badge
 * @author      Egils Eglitis <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Ltd (https://scandiweb.com)
 */

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'badge_id';
    protected $_eventPrefix = 'badges_collection';
    protected $_eventObject = 'post_collection';

    protected function _construct()
    {
        $this->_init('Scandi\Badge\Model\Post', 'Scandi\Badge\Model\ResourceModel\Post');
    }
}
