<?php namespace Scandi\Badge\Model;

/**
 * scandi_default
 *
 * @category    scandi
 * @package     scandi_badge
 * @author      Egils Eglitis <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Ltd (https://scandiweb.com)
 */

use Scandi\Badge\Model\ResourceModel\Post as PostResourceModel;
use Magento\Framework\Model\AbstractModel;

class Post extends AbstractModel
{
    protected function _construct()
    {
        $this->_init(PostResourceModel::class);
    }
}
