<?php namespace Scandi\Badge\Model;

/**
 * scandi_default
 *
 * @category    scandi
 * @package     scandi_badge
 * @author      Egils Eglitis <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Ltd (https://scandiweb.com)
 */

use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Scandi\Badge\Model\ResourceModel\Grid\CollectionFactory;

class DataProvider extends AbstractDataProvider
{
    protected $collection;
    protected $dataPersistor;
    protected $loadedData;
    protected $storeManager;

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $CollectionFactory,
        StoreManagerInterface $storeManager,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $CollectionFactory->create();
        $this->storeManager = $storeManager;
        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $meta,
            $data
        );
    }

    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        $items = $this->collection->getItems();

        foreach ($items as $block) {
            $badgeData = $block->getData();

            unset($badgeData['badge_image']);

            $badgeData['badge_image'][0]['name'] = $block->getBadgeImage();
            $badgeData['badge_image'][0]['url'] = $this->getMediaUrl().$block->getBadgeImage();

            $this->loadedData[$block->getId()] = $badgeData;
        }

        return $this->loadedData;
    }

    public function getMediaUrl()
    {
        $mediaUrl = $this->storeManager->getStore()
                ->getBaseUrl(UrlInterface::URL_TYPE_MEDIA).'badge/image/';
        return $mediaUrl;
    }
}
