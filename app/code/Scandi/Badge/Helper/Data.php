<?php namespace Scandi\Badge\Helper;

/**
 * scandi_default
 *
 * @category    scandi
 * @package     scandi_badge
 * @author      Egils Eglitis <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Ltd (https://scandiweb.com)
 */

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use Scandi\Badge\Model\Post;

class Data extends AbstractHelper
{
    private $post;
    private $storeManager;

    public function __construct(
        Context $context,
        Post $post,
        StoreManagerInterface $storeManager
    ) {
        $this->post = $post;
        $this->storeManager = $storeManager;
        parent::__construct($context);
    }

    public function getBadgeImageUrl($badgeId)
    {
        $badge = $this->post->load($badgeId);
        $badgeImageUrl = $this->storeManager->getStore()
                ->getBaseUrl(UrlInterface::URL_TYPE_MEDIA).'badge/image/'.$badge['badge_image'];

        return $badgeImageUrl;
    }

    public function getBadgeStatus($badgeId) {
        $badge = $this->post->load($badgeId);
        return $badge['badge_status'];
    }
}
