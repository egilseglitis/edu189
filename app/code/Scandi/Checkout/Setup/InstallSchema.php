<?php namespace Scandi\Checkout\Setup;
/**
  scandi_default

  @category    scandi
  @package     scandi_checkout
  @author      Egils Eglitis <info@scandiweb.com>
  @copyright   Copyright (c) 2018 Scandiweb, Ltd (https://scandiweb.com)
*/

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use \Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ){
        $installer = $setup;
        $installer->startSetup();

        $installer->getConnection()->addColumn(
            $installer->getTable('quote'),
            'personal_id_number',
            [
                'type' => Table::TYPE_TEXT,
                'length' => 12,
                'nullable' => false,
                'default' => '-',
                'comment' => 'Personal ID number for user'
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order'),
            'personal_id_number',
            [
                'type' => Table::TYPE_TEXT,
                'length' => 12,
                'nullable' => false,
                'default' => '-',
                'comment' => 'Personal ID number for user'
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order_grid'),
            'personal_id_number',
            [
                'type' => Table::TYPE_TEXT,
                'length' => 12,
                'nullable' => false,
                'default' => '-',
                'comment' => 'Personal ID number for user'
            ]
        );

        $setup->endSetup();
    }
}