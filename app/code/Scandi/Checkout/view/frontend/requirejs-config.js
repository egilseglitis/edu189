/**
  Scandi_default

  @category    Scandi
  @package     Scandi_checekout
  @author      Egils Eglitis <info@scandiweb.com>
  @copyright   Copyright (c) 2018 Scandiweb, Ltd (https://scandiweb.com)
*/

var config = {
    config: {
        mixins: {
            'Magento_Ui/js/lib/validation/validator': {
                'Scandi_Checkout/js/validation-mixin': true
            }
        }
    }
};
