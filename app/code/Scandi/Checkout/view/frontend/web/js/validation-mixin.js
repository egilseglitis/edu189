/**
 Scandi_default

 @category    Scandi
 @package     Scandi_checekout
 @author      Egils Eglitis <info@scandiweb.com>
 @copyright   Copyright (c) 2018 Scandiweb, Ltd (https://scandiweb.com)
 */

define(
    [
        'jquery',
        'jquery/ui',
        'jquery/validate',
        'mage/translate'
    ],
    function (
        $,
        validator
    ) {
        "use strict";

        return function (validator) {
            validator.addRule(
                'validate-personal-id-number',
                function (value) {
                    if (value.length === 12) {
                        return /\d{6}-\d{5}/.test(value);
                    }
                },
                $.mage.__('Please enter correct Personal ID Number.')
            );
            return validator;
        }
    }
);
