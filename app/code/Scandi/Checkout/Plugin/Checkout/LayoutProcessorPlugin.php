<?php namespace Scandi\Checkout\Plugin\Checkout;
/**
 * scandi_default
 *
* @category    scandi
* @package     scandi_checkout
* @author      Egils Eglitis <info@scandiweb.com>
* @copyright   Copyright (c) 2018 Scandiweb, Ltd (https://scandiweb.com)
 */

use Magento\Checkout\Block\Checkout\LayoutProcessor;

Class LayoutProcessorPlugin
{
    protected $AttributeCode = 'personal_id_number';

    public function afterProcess(
        LayoutProcessor $subject,
        array  $jsLayout
    ){
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
        ['shippingAddress']['children']['shipping-address-fieldset']['children'][$this->AttributeCode] = [
            'component' => 'Magento_Ui/js/form/element/abstract',
            'config' => [
                'customScope' => 'shippingAddress',
                'customEntry' => null,
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/input'
            ],
            'dataScope' => 'shippingAddress.' . $this->AttributeCode,
            'label' => 'Personal ID Number',
            'provider' => 'checkoutProvider',
            'sortOrder' => 0,
            'validation' => [
                'required-entry' => true,
                'validate-personal-id-number' => true
            ],
            'options' => [],
            'filterBy' => null,
            'customEntry' => null,
            'visible' => true,
        ];

        return $jsLayout;
    }


}