/**
 * Edu_default
 *
 * @category    Edu
 * @package     Edu_default
 * @author      Egils Eglitis <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Ltd (https://scandiweb.com)
 */

var config = {
    map: {
        '*': {
            'related-products': 'js/related-products',
            'sticky-header': 'js/sticky-header',
            'map': 'js/contact_us/map',
            'store': 'js/contact_us/store',
            'product-quantity': 'js/checkout/product-quantity',

            'Magento_Checkout/js/view/minicart': 'js/checkout/minicart',
            'sidebar': 'js/checkout/sidebar'
        }
    },

    deps: ['sticky-header']
};