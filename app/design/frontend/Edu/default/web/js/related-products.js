/**
 * Edu_default
 *
 * @category    Edu
 * @package     Edu_default
 * @author      Egils Eglitis <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Ltd (https://scandiweb.com)
 */

require(['jquery'], function($)
{
    $(document).ready(function ()
    {
        //Function to count after how many clicks to append/prepend another copy of related items
        function stepCount ()
        {
            switch (true) {
                case (viewValue < 768):
                    return productCount - 1;
                    break;
                case (viewValue >= 768 && viewValue < 1024):
                    return productCount - 2;
                    break;
                case (viewValue >= 1024 && viewValue < 1280):
                    return productCount - 3;
                    break;
                case (viewValue >= 1280 ):
                    return productCount - 4;
                    break;
            }
        }

        var animationDistance = 192;
        var animationDuration = 500;

        var clone;
        var relatedProducts = $('.related-products-list').children();

        var productCount = $('.item.product.product-item').length;
        var returnDistance = animationDistance * productCount;

        var viewValue = $(window).width();

        var leftProducts = 0;
        var rightProducts = stepCount();

        $('.block.related').append('<div class="related-products-nav-dots"></div>');
        $('.related-products-nav-dots').append('<ul></ul>');
        $('.related-products-nav-dots ul').append('<li class="nav-dot current-dot"></li>');

        for (var i = 0; i < productCount - 1; i++) {
            $('.related-products-nav-dots ul').append('<li class="nav-dot"></li>');
        }

        $('.related-arrow-left').on('click', function()
        {
            //If function to move nav dots
            if ($('.nav-dot:first').attr('class') === 'nav-dot current-dot') {
                $('.nav-dot.current-dot').removeClass('current-dot')
                $('.nav-dot:last').addClass('current-dot');
            } else {
                $('.nav-dot.current-dot').removeClass('current-dot').prev().addClass('current-dot');
            }

            //If function to append copy of related items if it's needed
            if (!leftProducts) {
                clone = relatedProducts.clone();
                $('.related-products-list').prepend(clone);
                $('.item.product.product-item').css('left', '-=' + returnDistance +'px');

                leftProducts = stepCount();
            } else {
                leftProducts--;
                rightProducts++;
            }

            $('.item.product.product-item').animate({left: '+=' + animationDistance +'px'}, animationDuration);
        });

        $('.related-arrow-right').on('click', function()
        {
            //If function to move nav dots
            if ($('.nav-dot:last').attr('class') === 'nav-dot current-dot') {
                $('.nav-dot.current-dot').removeClass('current-dot')
                $('.nav-dot:first').addClass('current-dot');
            } else {
                $('.nav-dot.current-dot').removeClass('current-dot').next().addClass('current-dot');
            }

            //If function to append copy of related items if it's needed
            if (!rightProducts) {
                clone = relatedProducts.clone();
                $('.related-products-list').append(clone);

                rightProducts = stepCount();
            } else {
                rightProducts--;
                leftProducts++;
            }

            $('.item.product.product-item').animate({left: '-=' + animationDistance +'px'}, animationDuration);
        })
    });
});
