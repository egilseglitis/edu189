/**
 * Edu_default
 *
 * @category    Edu
 * @package     Edu_default
 * @author      Egils Eglitis <info@scandiweb.com>
 * @copyright   Copyright (c) 2019 Scandiweb, Ltd (https://scandiweb.com)
 */

define([
    'jquery',
    'domReady!'
], function ($) {
    //In cart on button click increases/decreases product quantity
    $('.cart-qty-more-container, .cart-qty-less-container').on("click",function(){

        let inputField = $(this).siblings('.cart-qty-input').children('.input-text.qty');
        let currentQty = inputField.val();

        if ($(this).hasClass('cart-qty-more-container')) {
            inputField.val(++currentQty);
        } else if ($(this).hasClass('cart-qty-less-container')) {
            if (currentQty > 1) inputField.val(--currentQty);
        }
    });

});