/**
 * Edu_default
 *
 * @category    Edu
 * @package     Edu_default
 * @author      Egils Eglitis <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Ltd (https://scandiweb.com)
 */

require(['jquery'], function($)
{
    $(document).ready(function ()
    {
        //Function to slide the header up and down
        function slideHeader() {
            if (headerValve === 0){
                if ($(document).scrollTop() > stickyHeaderThreshold) {
                    headerValve = 1;
                    $('.top-header-container').animate({top: '-' + headerBannerHeight + 'px'}, slideDuration);
                    $('.header-nav-container').animate({top: '0px'}, slideDuration);
                    return;
                }
            } else if (headerValve === 1) {
                if ($(document).scrollTop() < stickyHeaderThreshold) {
                    headerValve = 0;
                    $('.top-header-container').animate({top: '0px'}, slideDuration);
                    $('.header-nav-container').animate({top: '' + headerBannerHeight + 'px'}, slideDuration);
                    return;
                }
            }
        }

        //Variable of the header banner height
        var headerBannerHeight = 145.125;

        //Variable, to get the viewport size to determine user device
        var viewValue = $(window).width();

        //Variable, to know, at how far down to slide up the header
        var stickyHeaderThreshold = 400;

        //Variable, to set sliding animation duration
        var slideDuration = 500;

        if (viewValue > 768) {
            //Variable to determine, if the user has scrolled down more than 400px
            var headerValve = 0;

            //If page is reloaded in the middle of a page, make top-header-container invisible to the user
            if ($(document).scrollTop() < stickyHeaderThreshold) {
                headerValve = 1;
                $('.top-header-container').addClass('visible-header-banner');
                slideHeader();
            } else if ($(document).scrollTop() > stickyHeaderThreshold) {
                headerValve = 1;
                $('.top-header-container').addClass('visible-header-banner');
            }

            $(window).on('scroll', function ()
            {
                slideHeader();
            }
            )
        }

    }
    )
}
);
