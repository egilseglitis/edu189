/**
 Scandi_StoreLocator

 @category    Scandi
 @package     Scandi_StoreLocator
 @author      Egils Eglitis <info@scandiweb.com>
 @copyright   Copyright (c) 2019 Scandiweb, Ltd (https://scandiweb.com)
 */

define([
    'jquery',
    'mage/translate'
], function ($) {
    'use strict';

    return function (config) {
        //Array of stores gotten from the form.phtml
        let storeArray = config.address;
        //Array to store list of store addresses
        let addressList = [];
        //Array to store list of store coordinates
        let coordinatesList = [];

        ymaps.ready(init);

        function init () {
            //Creates a new map
            let myMap = new ymaps.Map('map', {
                    center: [56.965675, 24.130154],
                    zoom: 12
                }, {
                    searchControlProvider: 'yandex#search'
                }),

                myGeoObject = new ymaps.GeoObject({
                    draggable: true
                });

            for (let i = 0; i < storeArray.length; ++i) {
                coordinatesList[i] = storeArray[i]['coord'].split(", ");
                addressList[i] = {coords: coordinatesList[i], text: storeArray[i]['store_address']};
                let storeInfo =
                    "<p class='store-name'><b>" + storeArray[i]['store_name'] + "</b></p>" +
                    "<p>" + storeArray[i]['store_address'] + "</p>" +
                    "<table>" +
                    "<tr><th>" + $.mage.__('Day') + "</th><th>" + $.mage.__('Time') + "</th></tr>" +
                    "<tr><td>" + $.mage.__('Monday') + "</td><td>" + storeArray[i]['monday_work_time'] + "</td></tr>" +
                    "<tr><td>" + $.mage.__('Tuesday') + "</td><td>" + storeArray[i]['tuesday_work_time'] + "</td></tr>" +
                    "<tr><td>" + $.mage.__('Wednesday') + "</td><td>" + storeArray[i]['wednesday_work_time'] + "</td></tr>" +
                    "<tr><td>" + $.mage.__('Thursday') + "</td><td>" + storeArray[i]['thursday_work_time'] + "</td></tr>" +
                    "<tr><td>" + $.mage.__('Friday') + "</td><td>" + storeArray[i]['friday_work_time'] + "</td></tr>" +
                    "<tr><td>" + $.mage.__('Saturday') + "</td><td>" + storeArray[i]['saturday_work_time'] + "</td></tr>" +
                    "<tr><td>" + $.mage.__('Sunday') + "</td><td>" + storeArray[i]['sunday_work_time'] + "</td></tr></table>";

                myMap.geoObjects
                    .add(myGeoObject)
                    .add(new ymaps.Placemark([coordinatesList[i][0], coordinatesList[i][1]], {
                        balloonContent: storeInfo,
                        iconContent: i+1
                    }, {
                        preset: 'islands#redIcon'
                    }));
            }
        }
    };
});