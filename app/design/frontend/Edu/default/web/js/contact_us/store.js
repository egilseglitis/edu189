/**
 Scandi_StoreLocator

 @category    Scandi
 @package     Scandi_StoreLocator
 @author      Egils Eglitis <info@scandiweb.com>
 @copyright   Copyright (c) 2019 Scandiweb, Ltd (https://scandiweb.com)
 */

require([
    'jquery',
    'jquery/ui',
    'domReady!'
], function ($) {
    let storeSearch = $('.store-search');
    let storeSearchResults = $('.store-search-results ul');

    storeSearch.on('input', function () {
        //Get Json array of Stores from the DB
        $.getJSON('http://edu.local/storelocator/action/storesearch', function (addressArray) {
            //User input value
            let check = storeSearch.val();
            check.toLowerCase();

            storeSearchResults.empty();

            addressArray.forEach(function (address) {
                //Remove dots, commas and spaces from the store address
                let addressTrim = address.replace(/[.,\s]/g, '').toLowerCase();

                //Seperates user input in separate words
                temp = check.split(' ');

                //Checker valve to make sure all words are in the address
                let valve = 0;

                for (let data of temp) {
                    if (addressTrim.match(data)) valve = 1;
                    else {
                        valve = 0;
                        break;
                    }
                }

                //If all words are in the Store address, then output it as a result
                if (valve) storeSearchResults.append('<li class="store-result">' + address + '</li>');
            })
        })
    });

    //On click of one of the options, autocomplete the input
    storeSearchResults.on('click', '.store-result', function () {
        storeSearch.val($(this).text());
    });

    let li;
    let liSelected;

    $(window).keydown(function (key) {
        li = storeSearchResults.find('li');

        switch (key.which) {
            //If Enter is pressed, autocomplete input with selected address
            case 13:
                if (storeSearchResults.find('.selected')) {
                    storeSearch.val(storeSearchResults.find('.selected').text());
                }

                break;

            //Address selection based on arrow inputs
            //If Up arrow is pressed
            case 38:
                if (liSelected) {
                    liSelected.removeClass('selected');
                    next = liSelected.next();

                    if (next.length > 0) {
                        liSelected = next.addClass('selected');
                    } else {
                        liSelected = li.eq(0).addClass('selected');
                    }

                } else {
                    liSelected = li.first().addClass('selected');
                }

                break;

            //If Down arrow is pressed
            case 40:
                if (liSelected) {
                    liSelected.removeClass('selected');
                    next = liSelected.prev();

                    if(next.length > 0){
                        liSelected = next.addClass('selected');
                    }else{
                        liSelected = li.last().addClass('selected');
                    }
                } else {
                    liSelected = li.first().addClass('selected');
                }

                break;
        }
    });
});