<?php
/**
 * Edu_default
 *
 * @category    Edu
 * @package     Edu_default
 * @author      Egils Eglitis <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Ltd (https://scandiweb.com)
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::THEME,
    'frontend/Edu/default',
    __DIR__
);